package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")

public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    @CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT})
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("getProducts");

        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);


        // Obtener listado de productos
        // Abrir una conexión a la BD
        // Gestionar conexcion a BD ( mirar si hay errores al conectar)
        // preparar una consulta a la BD
        // ejecutar la consulta
        // Procesar los resultados obtenidos ( o devolver error ( excepciones))
        // Ya con el listado de productos, mostrar al usuario / devolver resultado


    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProducts");
        System.out.println("La id del producto que se va a crear es " + product.getId());
        System.out.println("La descripcion  del producto que se va a crear es " + product.getDesc());
        System.out.println("El precio del producto que se va a crear es " + product.getPrice());

        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);

    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {
        System.out.println("getProducts");
        System.out.println("La id del producto a buscar  es " + id);

        Optional<ProductModel> result = this.productService.findById(id);
      //  Optional<ProductModel> result = // Llega Product Model;
      //  Optional<ProductModel> result = // no llega product model;

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel productModel, @PathVariable String id) {
        System.out.println("updateProducts");
        System.out.println("La id del producto que se va a actualizar en parámetro es " + id);
        System.out.println("La id del producto que se va a actualizar es " + productModel.getId());
        System.out.println("La descdel producto que se va a actualizar  es " + productModel.getDesc());
        System.out.println("el Price del producto que se va a actualizar  es " + productModel.getPrice());

        Optional<ProductModel> productToUpdate = this.productService.findById(id);

        if (productToUpdate.isPresent()){
            System.out.println("Producto para actualizar encontrado, actualizando");

                    this.productService.update(productModel);
        }

        return new ResponseEntity<>(productModel,
                productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }


    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto que se va a borrar es " + id);

        boolean deletedProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deletedProduct ? "Producto borrado" : "Producto no borrado",
                deletedProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );


    }
}