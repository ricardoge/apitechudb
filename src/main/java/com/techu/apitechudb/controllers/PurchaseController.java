package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")

public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchases")
    @CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT})
    public ResponseEntity<List<PurchaseModel>> getPurchases() {
        System.out.println("getPurchases");

        return new ResponseEntity<>(this.purchaseService.findAll(), HttpStatus.OK);


        // Obtener listado de purchaseos
        // Abrir una conexión a la BD
        // Gestionar conexcion a BD ( mirar si hay errores al conectar)
        // preparar una consulta a la BD
        // ejecutar la consulta
        // Procesar los resultados obtenidos ( o devolver error ( excepciones))
        // Ya con el listado de purchaseos, mostrar al usuario / devolver resultado


    }

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseModel> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchases");
        System.out.println("La id del purchase que se va a crear es " + purchase.getId());
        System.out.println("el userid  del purchase que se va a crear es " + purchase.getUserId());
        System.out.println("El precio del purchase que se va a crear es " + purchase.getAmount());
        System.out.println("Elementos de la compra del purchase que se va a crear es " + purchase.getPurchaseItems());

        return new ResponseEntity<>(this.purchaseService.add(purchase), HttpStatus.CREATED);

    }

    @GetMapping("/purchases/{id}")
    public ResponseEntity<Object> getPurchaseById(@PathVariable String id) {
        System.out.println("getPurchases");
        System.out.println("La id del purchasea buscar  es " + id);

        Optional<PurchaseModel> result = this.purchaseService.findById(id);
      //  Optional<PurchaseModel> result = // Llega Purchase Model;
      //  Optional<PurchaseModel> result = // no llega purchase model;

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "purchase no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/purchases/{id}")
    public ResponseEntity<PurchaseModel> updatePurchase(@RequestBody PurchaseModel purchaseModel, @PathVariable String id) {
        System.out.println("updatePurchases");
        System.out.println("La id del purchase que se va a actualizar en parámetro es " + id);
        System.out.println("La id del purchase que se va a actualizar es " + purchaseModel.getId());
        System.out.println("el user id del purchase que se va a actualizar  es " + purchaseModel.getUserId());
        System.out.println("el amount del purchase que se va a actualizar  es " + purchaseModel.getAmount());

        Optional<PurchaseModel> purchaseToUpdate = this.purchaseService.findById(id);

        if (purchaseToUpdate.isPresent()){
            System.out.println("purchase para actualizar encontrado, actualizando");

                    this.purchaseService.update(purchaseModel);
        }

        return new ResponseEntity<>(purchaseModel,
                purchaseToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }


    @DeleteMapping("/purchases/{id}")
    public ResponseEntity<String> deletePurchase(@PathVariable String id) {
        System.out.println("deletePurchase");
        System.out.println("La id del purchase que se va a borrar es " + id);

        boolean deletedPurchase = this.purchaseService.delete(id);

        return new ResponseEntity<>(
                deletedPurchase ? "purchase borrado" : "purchase no borrado",
                deletedPurchase ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );


    }
}