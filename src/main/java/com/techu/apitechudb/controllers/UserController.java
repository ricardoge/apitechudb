package com.techu.apitechudb.controllers;


import com.mongodb.internal.operation.OrderBy;
import com.techu.apitechudb.models.UserModel;
        import com.techu.apitechudb.services.UserService;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.http.HttpStatus;
        import org.springframework.http.ResponseEntity;
        import org.springframework.web.bind.annotation.*;

        import java.util.List;
        import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT})

public class UserController {

    @Autowired
    UserService userService;



    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("addUsers");
        System.out.println("La id del user que se va a crear es " + user.getId());
        System.out.println("La descripcion  del user que se va a crear es " + user.getName());
        System.out.println("El precio del user que se va a crear es " + user.getAge());

        return new ResponseEntity<>(this.userService.add(user), HttpStatus.CREATED);

    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("getUsers");
        System.out.println("La id del user a buscar  es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "User no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel userModel, @PathVariable String id) {
        System.out.println("updateUsers");
        System.out.println("La id del user que se va a actualizar en parámetro es " + id);
        System.out.println("La id del user que se va a actualizar es " + userModel.getId());
        System.out.println("La descdel user que se va a actualizar  es " + userModel.getName());
        System.out.println("el Price del user que se va a actualizar  es " + userModel.getAge());

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if (userToUpdate.isPresent()){
            System.out.println("User para actualizar encontrado, actualizando");
            this.userService.update(userModel);
        }

        return new ResponseEntity<>(userModel,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }


    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUseºr(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("La id del user que se va a borrar es " + id);

        boolean deletedUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deletedUser ? "User borrado" : "User no borrado",
                deletedUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );


    }


    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam (name = "$orderBy", required = false) String orderBy
    ) {
        System.out.println("getUsers");
        System.out.println("El valor de $orderBy es " + orderBy);
        return new ResponseEntity<>(this.userService.getUsers(orderBy), HttpStatus.OK);
    }

}