package com.techu.apitechudb.services;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;
    UserRepository userRepository;

    public List<PurchaseModel> findAll() {

        return this.purchaseRepository.findAll();
    }

    public PurchaseModel addPurchase(PurchaseModel purchase) {
        System.out.println("addPurchase");
        return this.purchaseRepository.save(purchase);

    }
    public Optional<PurchaseModel> findById(String id) {
        System.out.println("findbyId");
        return this.purchaseRepository.findById(id);

    }

    public PurchaseModel update(PurchaseModel purchase) {
        System.out.println("update");
        return this.purchaseRepository.save(purchase);

    }

    public boolean delete(String id){
        System.out.println("delete");

        boolean result = false;

        if (this.purchaseRepository.findById(id).isPresent() == true) {
            System.out.println("Purchase encontrado, borrando");
            this.purchaseRepository.deleteById(id);

            result = true;

        }
        return result;
    }


}
